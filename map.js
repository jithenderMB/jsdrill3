const map = (elements, cb) => {
    const newArray = [];
    if (!elements || !cb) {return newArray;}
    for (let index=0; index<elements.length; index++){
        let element = elements[index];
        newArray.push(cb(element,index,elements));
    }
    return newArray;
};

module.exports = map;