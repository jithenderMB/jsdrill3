const each = require("../each")


const items = [1, 2, 3, 4, 5, 5]; 
const result = each(items,(item,index) => {
    console.log(`${item} is at index ${index}`);
});