const flatten = require("../flatten")

let nestedArray = [1, [2], [3, [[4]]]] ;
let depth = 3;
let result = flatten(nestedArray,depth);
console.log(result);