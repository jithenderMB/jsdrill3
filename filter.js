const filter = (elements, cb) => {
    const newArray = [];
    if (!elements || !cb) {newArray;}
    for (let index=0;index<elements.length;index++){
        let element = elements[index];
        if (cb(element, index, elements)) {newArray.push(element);}
    }
    return newArray;
};

module.exports = filter;