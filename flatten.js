const flat=(elements)=>{
    let newArray=[];
    for (let element of elements){
        if (Array.isArray(element)){
            newArray.push(...element);
        }
        else{
            newArray.push(element);
        }
    }
    return newArray;
};
const flatten = (elements,depth) => {
    if(!elements) {return [];}
    if (!depth){depth =1;}    
    for (let i=0;i<depth;i++){
        elements = flat(elements);
    }
    return elements;
};
module.exports = flatten;
