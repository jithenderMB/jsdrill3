function each(elements, cb){
    if (!elements || !cb) return [];
    for (let index=0; index<elements.length; index++){
        let element = elements[index];
        cb(element,index,elements);
    }
}

module.exports = each;

