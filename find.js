const find = (elements, cb,startIndex) =>{
    let start = 0;
    if (!elements || !cb) return [];
    if (startIndex) start = startIndex;
    for (let i = start;i<elements.length;i++){
        let response = cb(elements[i]);
        if (response) return response;
    }
    return undefined;
};

module.exports = find;