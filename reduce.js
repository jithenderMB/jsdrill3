const reduce = (elements, cb, startingValue) =>{
    if (!elements || !cb) {return [];}
    let previousValue;
    let index = 0;
    if (startingValue) { previousValue = startingValue;}
    else {
        index = 1;
        previousValue = elements[0];
    }
    for (index; index<elements.length; index++){
        let currentValue = elements[index];
        previousValue = cb(previousValue, currentValue, index, elements);
    }
    return previousValue;
};

module.exports = reduce;